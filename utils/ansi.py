from typing import Literal, get_args

__all__ = ["ColorString", "cursor", "erase"]

ESC = "\x1b"

COLOR = Literal[
    "black",
    "red",
    "green",
    "yellow",
    "blue",
    "magenta",
    "cyan",
    "white",
    "light black",
    "light red",
    "light green",
    "light yellow",
    "light blue",
    "light magenta",
    "light cyan",
    "light white",
]

MODE = Literal[
    "bold",
    "dim",
    "italic",
    "underline",
    "blinking",
    "inverse",
    "hidden",
    "strikethrough",
]


class ColorString:
    _colors: dict[COLOR, int] = {
        "black": 0,
        "red": 1,
        "green": 2,
        "yellow": 3,
        "blue": 4,
        "magenta": 5,
        "cyan": 6,
        "white": 7,
        "light black": 8,
        "light red": 9,
        "light green": 10,
        "light yellow": 11,
        "light blue": 12,
        "light magenta": 13,
        "light cyan": 14,
        "light white": 15,
    }

    _mode_dict: dict[MODE, int] = {
        "bold": 1,
        "dim": 2,
        "italic": 3,
        "underline": 4,
        "blinking": 5,
        "inverse": 6,
        "hidden": 7,
        "strikethrough": 8,
    }

    def __init__(
        self,
        string: str,
        fg: COLOR | int | None = None,
        bg: COLOR | int | None = None,
        modes: list[MODE] | None = None,
    ):
        self._data = string
        self._fg = ""
        self._bg = ""
        self._modes = []

        if fg:
            self.fg(fg)
        if bg:
            self.bg(bg)
        if modes:
            self.mode(*modes)

    def fg(self, color: COLOR | int):
        color = self._colors.get(color, color)
        self._fg = f"{ESC}[38;5;{color}m"
        return self

    def bg(self, color: COLOR | int):
        color = self._colors.get(color, color)
        self._bg = f"{ESC}[48;5;{color}m"
        return self

    def mode(self, *modes: MODE, clear: bool = False):
        if clear:
            self._modes = []
        for mode in modes:
            self._modes += [f"{ESC}[{self._mode_dict[mode]}m"]
        return self

    @property
    def data(self):
        return self._data

    def __str__(self):
        val = "".join(self._modes)
        val += f"{self._bg}{self._fg}{self._data}"

        if self._fg or self._bg or self._modes:
            val += f"{ESC}[0m"

        return val

    def __len__(self):
        return len(self._data)


class cursor:
    def __new__(cls):
        return TypeError("Static Class")

    @staticmethod
    def up(self, n: int = 1):
        print(f"{ESC}[{n}A", end="")
        return self

    @staticmethod
    def down(self, n: int = 1):
        print(f"{ESC}[{n}B", end="")
        return self

    @staticmethod
    def left(self, n: int = 1):
        print(f"{ESC}[{n}C", end="")
        return self

    @staticmethod
    def right(self, n: int = 1):
        print(f"{ESC}[{n}D", end="")
        return self

    @staticmethod
    def next_line(self, n: int = 1):
        print(f"{ESC}[{n}E", end="")
        return self

    @staticmethod
    def prev_line(self, n: int = 1):
        print(f"{ESC}[{n}F", end="")
        return self

    @staticmethod
    def to(self, col: int = 0, line: int = 0):
        print(f"{ESC}[{line};{col}H", end="")
        return self

    @staticmethod
    def show(self):
        print(ESC + "[?25h", end="")
        return self

    @staticmethod
    def hide(self):
        print(ESC + "[?25l", end="")
        return self


class erase:
    def __new__(cls):
        return TypeError("Static Class")

    @staticmethod
    def end_line(self):
        print(f"{ESC}[0K", end="")
        return self

    @staticmethod
    def start_line(self):
        print(f"{ESC}[1K", end="")
        return self

    @staticmethod
    def line(self):
        print(f"{ESC}[2K", end="")
        return self

    @staticmethod
    def up(self):
        print(f"{ESC}[0J", end="")
        return self

    @staticmethod
    def down(self):
        print(f"{ESC}[1J", end="")
        return self

    @staticmethod
    def screen(self):
        print(f"{ESC}[2J", end="")
        return self


def test_codes():
    for i in range(256):
        print(ColorString(f"{i:<3}", fg=i), end=" ")
    print()
    print()

    for i in range(256):
        print(ColorString(f"{i:<3}", bg=i), end=" ")
    print()
    print()

    for c in get_args(COLOR):
        print(ColorString(f"{c:<15}", fg=c), end=" ")
        print(ColorString(f" {c:<15} ", bg=c))

    print()

    for m in get_args(MODE):
        print(ColorString(f"{m:<3}", modes=[m]), end=" ")

    print()
    print()


if __name__ == "__main__":
    test_codes()
