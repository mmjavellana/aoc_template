import pathlib
from abc import ABC, abstractmethod

from utils.ansi import ColorString as cs
from . import _AOCDays as AOCDays


def day(day_number: int, name: str = None):
    def day_decorator(cls: aoc):
        if not str(cls.__module__).replace("days.", "").startswith("_"):
            AOCDays.add_day(day_number, cls(day_number, name))
        return cls

    return day_decorator


class aoc(ABC):
    def __init__(
        self,
        day_number: int,
        name: str = None,
    ):
        self.day_number = day_number
        self.name = name

        self.output_filename = pathlib.Path(f"outputs/{day_number:02}.txt").absolute()
        self.output_filename.parent.mkdir(parents=True, exist_ok=True)

    def run(
        self,
        input_data: list[str] = [],
    ):
        header = f"Day {self.day_number}"
        header += f": {cs(self.name, "green")}" if self.name else ""

        print(
            cs("===", "red"),
            header,
            cs("===", "red"),
        )
        self.common(input_data)
        print()

        print(f"{cs("Part 1: ", "cyan")}{self.part1(input_data)}")
        print()

        print(f"{cs("Part 2: ", "cyan")}{self.part2(input_data)}")
        print()

    @abstractmethod
    def common(self, input_data: list[str]):
        pass

    @abstractmethod
    def part1(self, input_data: list[str]):
        pass

    @abstractmethod
    def part2(self, input_data: list[str]):
        pass
