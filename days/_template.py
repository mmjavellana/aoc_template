from ._util import aoc, day
from utils.decorators import time_it

day_number = 0
name = None


@day(day_number, name)
class DaySolution(aoc):
    def common(self, input_data):
        pass

    @time_it
    def part1(self, input_data):
        pass

    @time_it
    def part2(self, input_data):
        pass


if __name__ == "__main__":
    DaySolution(day_number).run()
