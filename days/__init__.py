from os import path
from glob import glob

from . import _AOCDays as AOCDays

__all__ = ["AOCDays"]

days = filter(
    lambda x: not path.basename(x).startswith("_"),
    glob(path.dirname(__file__) + "/[0-2][0-9].py"),
)

__all__.extend(path.basename(f)[:-3] for f in days)
