from ._util import aoc

_days = {}


def add_day(number: int, cls: "aoc") -> None:
    _days[number] = cls


def get_day(number: int) -> "aoc":
    return _days.get(number, None)


def get_days() -> list[int]:
    return [*_days.keys()]
