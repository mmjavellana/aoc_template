#!/usr/bin/env python
from datetime import datetime, timezone, timedelta
from importlib import import_module
from pathlib import Path
from time import sleep

import argparse
import json
import requests
import sys
import traceback

from days import *  # noqa: F403,F401
from days import AOCDays

from utils.ansi import ColorString as cs, cursor, erase

PREFIX_ERR = cs(" ERR! ", bg="red")
PREFIX_WARN = cs(" WARN ", bg="yellow")
PREFIX_INFO = cs(" INFO ", bg="blue")

EST = timezone(timedelta(hours=-5))

YEAR = datetime.now(EST).year
SESSION_TOKEN = ""


def _load_settings():
    settings_file = Path("settings.json")
    if not settings_file.is_file():
        settings_file.write_bytes(Path("settings.json.default").read_bytes())
        print(PREFIX_WARN, "Please fill in settings.json first!")
        sys.exit(3)

    with settings_file.open("r") as s:
        settings = json.loads("".join(s.readlines()))

        global YEAR, SESSION_TOKEN
        YEAR = settings["year"]
        SESSION_TOKEN = settings["session_token"]


_load_settings()


def main():
    args = parse_args()

    days_to_run = AOCDays.get_days() if args.all else args.day
    print(PREFIX_INFO, "Attempting to run AoC day(s)...")
    print()
    for day in days_to_run:
        run_day(
            day,
            args.filename,
            args.invalidate_cache,
        )


def get_input_from_cache(
    year: int,
    day: int,
    invalidate_cached_input: bool = False,
) -> list[str]:
    waiting = 0
    while True:
        now = datetime.now(EST)
        delta = datetime(year, 12, day, tzinfo=EST) - now

        if waiting:
            cursor.hide().up()
            erase.line()
            cursor.show()

        if delta < timedelta(seconds=0):
            break

        countdown = str(delta).split(".")[0]
        print(
            PREFIX_WARN,
            f"Puzzle has not unlocked yet. {countdown} remaining ",
            "" if delta.days else f"[{f"{"":*>{10 - abs(9 - waiting % 18)}}":10}]",
        )

        if delta > timedelta(days=1):
            sys.exit(3)

        waiting += 1
        sleep(1)

    cached_file = Path(f"./inputs/{day:02}.txt").absolute()
    cached_file.parent.mkdir(parents=True, exist_ok=True)
    if not cached_file.is_file() or invalidate_cached_input:
        print(
            PREFIX_INFO,
            f"Downloading input for day {day} from adventofcode.com",
        )

        if not SESSION_TOKEN:
            print(PREFIX_ERR, "No session token")
            sys.exit(3)

        result = requests.get(
            f"https://adventofcode.com/{year}/day/{day}/input",
            headers={
                "cookie": f"session={SESSION_TOKEN}",
                "User-Agent": (
                    f"https://bitbucket.org/mmjavellana/aoc_{year}/"
                    f" by {"mmjavellana"}@{"gmail"}.com"
                ),
            },
        )
        if result.status_code == 200:
            with cached_file.open("w") as f:
                f.write(result.text)
        else:
            raise ConnectionError(
                "Could not connect to AoC website to download input data. "
                f"Error code {result.status_code}: {result.text}"
            )

    return load_input_from_file(cached_file)


def load_input_from_file(filename: str) -> list[str]:
    if Path(filename).is_file():
        with open(filename, "r") as f:
            return f.read().splitlines()


def run_day(
    day: int,
    filename: str,
    invalidate_cache: bool,
) -> None:
    if sys.stdin.isatty():
        if filename:
            input_data = load_input_from_file(filename)
        else:
            input_data = get_input_from_cache(YEAR, day, invalidate_cache)
    else:
        print(
            PREFIX_WARN,
            "Standard input isn't interactive; assuming input data is piped",
        )
        input_data = sys.stdin.read().splitlines()

    aocday = AOCDays.get_day(day)
    if not aocday:
        # Create the day's files
        print(PREFIX_INFO, f"Creating file for day {day}")

        template = Path("days/_template.py")
        newday = Path(f"days/{day:02}.py")

        lines = template.read_text()
        lines = lines.replace("day_number = 0", f"day_number = {day}")

        newday.write_text(lines)

        import_module(f"days.{day:02}")
        aocday = AOCDays.get_day(day)

    if aocday:
        try:
            aocday.run(input_data)
        except ConnectionError as e:
            print(e, file=sys.stderr)
        except Exception:
            traceback.print_exc()


def parse_args():
    parser = argparse.ArgumentParser(
        prog="run.py",
        description="Runs a given AOC day",
    )
    parser.add_argument(
        "day",
        nargs="*",
        type=day_before_christmas,
        help="The day to run. Defaults to the current day. Valid options are 1 - 25.",
    )

    group = parser.add_mutually_exclusive_group()
    group.add_argument("-f", "--filename", help="The input filename to use.")
    group.add_argument(
        "-a", "--all", action="store_true", help="Include to run all valid days."
    )

    parser.add_argument(
        "--invalidate-cache",
        action="store_true",
        help="Invalidate the cached input; fetch again from the server.",
    )

    args = parser.parse_args()
    if args.day:
        args.day = sorted({*args.day})
    else:
        args.day = [min(25, datetime.now(EST).day)]

    return args


def day_before_christmas(value):
    day = int(value)
    if day <= 0 or day >= 26:
        raise argparse.ArgumentTypeError(f"{value} is out of the range 1 - 25.")
    return day


if __name__ == "__main__":
    main()
